var background_page = chrome.extension.getBackgroundPage();
chrome.tabs.getSelected(null, function(tab) {
	$('#content_block').css('color', '#000000');
	chrome.extension.sendRequest({method: "getShortURL", taburl: tab.url}, function(response) {
		var protocol =  response.data.match(/^(.[^:]+):/);
		if(protocol == "http:,http") {
			$('#content_text').html('Url gekopieerd in klembord: '+response.data);
		} else {
			$('#content_text').html('Error: '+response.data);
		}
		setTimeout(function() {
			window.close();
    }	, 5000);
		console.log(response);
	});
	//$('#content_block').css('background-color', '#336600');
	$('#content_block').width(180);
	$('#content_close').show();
	$('#content_close').click(function () {
		window.close();
	});
});

$(document).ready(function() {
});

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-25070052-1']);
_gaq.push(['_trackPageview', '/extension/shorturl']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = 'https://ssl.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();