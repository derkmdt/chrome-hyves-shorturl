chrome.tabs.onActivated.addListener(function(tab_obj){
	chrome.tabs.get(tab_obj.tabId, function(tab) {
		showButton(tab);
	});
});
chrome.tabs.onUpdated.addListener(function(tabId, change, tab) {
	if ( change.status == "loading" ) {
		showButton(tab);
	}
});
var showButton = function(tab) {
    if(!tab) return;
    var protocol =  tab.url.match(/^(.[^:]+):/)[1];
	//console.log('protocol: '+protocol);
	if(protocol == "https" || protocol == "http") {
		chrome.browserAction.setIcon({tabId:tab.id, path:"img/icon-32.png"});
	} else {
		chrome.browserAction.setIcon({tabId:tab.id, path:"img/icon-32-grey.png"});
	}
};
chrome.extension.onRequest.addListener(
	function(request, sender, sendResponse){
		console.log(request.taburl);
		if(request.method == 'getShortURL') {
			console.log('shortenUrl:');
			shortenUrl(request.taburl, function(result) {
				sendResponse({data: result});
			});
		}
	}
);
var shortenUrl = function(longUrl, callback) {
	hyves =  new Hyves(hyves_api_key, hyves_api_secret);
	hyves.call('shorturl.create', {'url': longUrl}, function(response) {
		if(response._response.error_code) {
			console.log(response._response.error_message);
			callback(response._response.error_message);
		} else {
			copyToClipboard(response._response.shorturl[0].shorturl);
			callback(response._response.shorturl[0].shorturl);
		}
	});
}
function copyToClipboard(text) {
	var input = document.getElementById('url');
	
	if(input == undefined)
		return;
	
	input.value = text;					
	input.select();

	document.execCommand('copy', false, null);
}